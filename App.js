import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity,ScrollView } from "react-native";

const Flex = () => {
  return (
    <View style={[styles.container, {
      // Try setting `flexDirection` to `"row"`.
      flexDirection: "column"
    }]}>
      <View style={styles.navbar }>
      <Image source={require('./image/tube.png')} style={{width: 100, height: 20}}/>
        <View style={styles.rightnav}>
        <TouchableOpacity>
        <Image source={require('./image/th.jpg')} style={ styles.navitem}/>
        </TouchableOpacity>
        <TouchableOpacity>
        <Image source={require('./image/pot.jpg')} style={ styles.navitem}/>
        </TouchableOpacity>
      </View>
      </View>
      <ScrollView>
      <View style={styles.content}>
        <TouchableOpacity>
       <Image source={require('./image/ricis.jpg')} style={{width: 390, height: 200}}/>
       </TouchableOpacity>
       <View style={styles.view}>
       <TouchableOpacity>
       <Image source={require('./image/ria.jpg')} style={{width: 40, height: 40,borderRadius: 25}}/>
       </TouchableOpacity> 
       <Text style={styles.kata}>TETEP RUSUH DI MADINAH- RIA RICIS (VLOG9)</Text>
       <TouchableOpacity>
       <Image source={require('./image/titik.jpg')} style={{width: 19, height: 19, flexDirection: 'row'}}/></TouchableOpacity>
       </View>
       <Text style={styles.teks}>Ricis Official 7,5jt x ditonton 4 tahun yang lalu</Text>
       <TouchableOpacity>
       <Image source={require('./image/quran.jpg')} style={{width: 390, height: 225}}/>
       </TouchableOpacity>
       <View style={styles.view}>
       <TouchableOpacity>
       <Image source={require('./image/hafiz.jpg')} style={{width: 40, height: 40,borderRadius: 25}}/>
       </TouchableOpacity> 
       <Text style={styles.kata}>HAFIZ INDONESIA | Aku Hafiz Quran dinyanyikan oleh peserta hafiz</Text>
       <TouchableOpacity>
       <Image source={require('./image/titik.jpg')} style={{width: 19, height: 19, flexDirection: 'row'}}/></TouchableOpacity>
       </View>
       <Text style={styles.teks}>Hafiz Indonesia 8,5jt x ditonton 2 minggu yang lalu</Text>
       <TouchableOpacity>
       <Image source={require('./image/mtk.jpg')} style={{width: 390, height: 200}}/></TouchableOpacity>
       <View style={styles.view}>
       <TouchableOpacity>  
       <Image source={require('./image/jpg.jpg')} style={{width: 50, height: 49,borderRadius: 25}}/></TouchableOpacity>
       <Text style={styles.kata}>SOAL CPNS? JEROME DAN BILLY BISA NGERJAIN DIBAWAH 30 DETIK</Text>
       <TouchableOpacity>
       <Image source={require('./image/titik.jpg')} style={{width: 19, height: 19,flexDirection: 'row'}}/></TouchableOpacity>
       </View>
       <Text style={styles.teks}>Nihongo Mantappu  2,3jt x ditonton  1 tahun yang lalu</Text>
       <TouchableOpacity>
       <Image source={require('./image/najwa.jpg')} style={{width: 390, height: 200}}/>
       </TouchableOpacity>
       <View style={styles.view}>
       <TouchableOpacity> 
       <Image source={require('./image/shihab.jpg')} style={{width: 50, height: 49,borderRadius: 25}}/></TouchableOpacity>
       <Text style={styles.kata}>UNTUK SISWA YANG LULUS TAHUN 2020 CATATAN NAJWA</Text>
       <TouchableOpacity>
       <Image source={require('./image/titik.jpg')} style={{width: 19, height: 19, flexDirection: 'row'}}/></TouchableOpacity>
       </View>
       <Text style={styles.teks}>Najwa Shihab  1,3jt x ditonton  10 bulan yang lalu</Text>
       <TouchableOpacity>
       <Image source={require('./image/tur.jpg')} style={{width: 390, height: 200}}/>
       </TouchableOpacity>
       <View style={styles.view}>
       <TouchableOpacity> 
       <Image source={require('./image/mansur.jpg')} style={{width: 50, height: 49,borderRadius: 25}}/></TouchableOpacity> 
       <Text style={styles.kata}>TOUR KELILING PONDOK PESANTREN MEVVAHH! ADA KOLAM RENANG DAN COFFESHOP</Text>
       <TouchableOpacity>
       <Image source={require('./image/titik.jpg')} style={{width: 19, height: 19}}/></TouchableOpacity>
       </View>
       <Text style={styles.teks}>Wirda Mansur  4,3jt x ditonton  5 tahun yang lalu</Text>
      </View>
      </ScrollView>
      <View style={styles.tabbar}>
      <TouchableOpacity style={styles.item}>
        <Image source={require('./image/home.jpg')} style={{width: 40, height: 40}}/>
        <Text style={styles.text}>Home</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.item}>
        <Image source={require('./image/api.jpg')} style={{width: 40, height: 40}}/>
        <Text style={styles.text}>Trending</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.item}>
        <Image source={require('./image/sb.jpg')} style={{width: 40, height: 40}}/>
        <Text style={styles.text}>Subscription</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.item}>
        <Image source={require('./image/notif.jpg')} style={{width: 40, height: 40}}/>
        <Text style={styles.text}>Notifikasi</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.item}>
        <Image source={require('./image/capt.jpg')} style={{width: 40, height: 40}}/>
        <Text style={styles.text}>Library</Text>
      </TouchableOpacity>
      </View>
      </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    },
  navbar:{
    height: 55,
    backgroundColor: 'white',
    elevation:3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center' ,
    justifyContent: 'space-between',
   },
   rightnav:{
     flexDirection: 'row',
   },
   navitem:{
     width: 40,
     height: 40,
     marginLeft: 25,
   },
   content:{
     flex: 1,
     padding: 10,
   },
   tabbar:{
     backgroundColor: 'white',
     height: 60,
     borderTopWidth: 0.5, 
     borderColor: '#E5E5E5',
     flexDirection: 'row',
     justifyContent: 'space-around',   
    },
    item:{
      alignItems: 'center',
      justifyContent: 'center',
    },
    text:{
      fontSize: 11,
      color: 'black',
      },
     view: {
       flexDirection: 'row',
       paddingTop: 13,
     },
     kata: {
       fontSize: 16,
       color: 'black',
       paddingHorizontal: 10,
       flexDirection: 'row',
     },
     teks: {
       paddingHorizontal: 60,
       flex: 1,
     }
});

export default Flex;